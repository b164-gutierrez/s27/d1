// Node.js Routing w/ HTTP Methods 03-28-22
// Activity

// Create a mock database with the data:

const http = require('http');

const port = 6000;

let directory = [
	{
		"firstName": "Mary Jane",
		'lastName': 'Dela Cruz',
		'mobileNo': '09123456789',
		"email": "mjdelacruz@mail.com",
		'password': 123
	},
	{
		"firstName": "John",
		'lastName': 'Doe',
		'mobileNo': '09123456789',
		"email": "jdoe@mail.com",
		'password': 123
	}
]

// Create a route "/profile" and request all the information in the data.
console.log(typeof directory);
const server = http.createServer((req, res) => {

	if(req.url == '/profile' && req.method == 'GET'){
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.write(JSON.stringify(directory));
		res.end()
		
	}

	if(req.url == '/profile' && req.method == 'POST'){
		let requestBody = '';
		req.on('data', function(data){
			requestBody += data;
		})

				req.on('end', function(){
			console.log(typeof requestBody);

			
			requestBody = JSON.parse(requestBody)

			
			let newProfile = {
				"name": requestBody.name,
				"email": requestBody.email
			}

			// add the new user into the mock database
			directory.push(newProfile);
			console.log(directory)

			res.writeHead(200, {'Content-Type': 'application/json'});
			res.write(JSON.stringify(newProfile));
			res.end();
		})
	}

	});

server.listen(port);

console.log(`Server running at localhost:${port}`);