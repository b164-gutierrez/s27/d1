			// Node.js Routing w/ HTTP Methods 03-28-22



// the method 'GET' means that we will be retrieving or reading an information.

const http = require('http');
const port = 4000

const server = http.createServer((req, res) => {

		//GET Method
		if(req.url =='/items' && req.method == 'GET'){
			res.writeHead(200, {'Content-Type': 'text/plain'});
			res.end('Data retrieved from the database')
		}

		//POST Method
		if(req.url == '/items' && req.method == 'POST'){
			res.writeHead(200, {'Content-Type': 'text/plain'});
			res.end('Data to be sent to the database');
		}

		//PUT Method
		if(req.url == '/updateItems' && req.method == 'PUT'){
			res.writeHead(200, {'Content-Type': 'text/plain'});
			res.end('Update our resources');
		}

		//DELETE Method
		if(req.url == "/delete" && req.method == "DELETE"){
			res.writeHead(200, {'Content-Type': 'text/plain'});
			res.end('Delete our resources');
	}
})

server.listen(port);
console.log(`Server is Running at localhost: ${port}`);