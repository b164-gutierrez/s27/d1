const http = require('http');

const port = 6000;

//Mock database
let directory = [
	{
		"name": "Brandon",
		"email": "brand@mail.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@mail.com"
	}
]

console.log(typeof directory);
const server = http.createServer((req, res) => {

	//route for returning ALL items upon receiving GET request.
	if(req.url == '/users' && req.method == 'GET'){
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.write(JSON.stringify(directory));
		res.end()
		
	}

	//Create users
	if(req.url == '/users' && req.method == 'POST'){
		// declare and initialize a 'requestBody' variable to an empty string.

		//this will act as a placeholder for the resource/data to created later on.
		let requestBody = '';

		// A 'stream' is a sequence of data

		//1. data step - first sequence of stream. this reads the 'data' stream process it as the request body.
		//information provided from the request object(client) enters a sequence of 'data'.

		req.on('data', function(data){
			// asssigns the data from the data stream to requestBody.
			requestBody += data;
		})

		// response and step - only runs after the request has completely been sent.
		req.on('end', function(){
			console.log(typeof requestBody);

			// convert the string requestBody to JSON
			requestBody = JSON.parse(requestBody)

			// create new object reprensenting the new mock database record
			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			}

			// add the new user into the mock database
			directory.push(newUser);
			console.log(directory)

			res.writeHead(200, {'Content-Type': 'application/json'});
			res.write(JSON.stringify(newUser));
			res.end();
		})
	}







});

server.listen(port);

console.log(`Servere running at localhost:${port}`);